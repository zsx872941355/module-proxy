import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server 
{
	static final int PORT = 21230;
	
	public static void main(String[] args) throws IOException 
	{
		new Server().serverStart();
	}

	private void serverStart() throws IOException
	{
		ServerSocket server = new ServerSocket(PORT);
		System.out.println("Server start at port " + PORT);
		while (true)
		{
			Socket socket = server.accept();
			
			new Process(socket).start();
		}
	}
}
