package aaa;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class BBB 
{
	public String hello(Object data) throws Exception
	{
		Map<String, Object> map = (Map) data;
		map.put("time", now());
		map.put("module", "java");
		
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(map);
	}
	
	String now()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date());
	}
}
