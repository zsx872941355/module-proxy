![Module Proxy](https://images.gitee.com/uploads/images/2021/1030/205651_a746dc49_1896879.png "logo.png")

Module Proxy是一款HTTP反向代理中间件，突出的靓点是将HTTP协议代理为TCP Socket协议，特别适合Web前后台分离的编程架构项目，使用Module Proxy可以让后端编程从HTTP技术体系中抽身出来，这将带来两个重要的改变：
- 无Web编程经验的程序员，可以轻松进行B/S后端的编程工作。
- 几乎所有的现代编程语言，都可以被使用进行B/S后端的编程工作，使用中不需要这些语言有HTTP的框架实现。

Module Proxy中间件由Rust语言实现，使用了优秀的异步运行时Tokio和HTTP底层库hyper，具有高效、稳定、内存占用低的特性。

#### 架构图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1030/214300_f58953f6_1896879.png "架构图1.png")

Module Proxy符合标准的http协议，可作为以下三种不同的服务器来使用：

- HTTP静态网站服务器
- HTTP反向代理服务器
- Socket代理转发服务器

#### 源代码编译

编译前需要先安装Rust语言环境，编译时只需要下载仓库中的Cargo.toml文件和src目录及下面的所有.rs文件即可。

编译命令  **cargo build --release**  

在Linux系统中编译后可得到二进制执行文件 modproxy ， 在windows系统中编译后得到 modproxy.exe 。

Rust语言的环境安装和编译等相关信息，请参考Rust官方网站 [https://www.rust-lang.org](https://www.rust-lang.org)

#### 发行版程序包

已提供编译好的发行版程序包，可直接下载使用：

- [https://gitee.com/dyf029/module-proxy/releases](https://gitee.com/dyf029/module-proxy/releases)

发行版提供zip和tar.gz两种打包形式，建议在linux上使用.tar.gz程序包来保持可执行文件的可执行属性。

解压程序包后，有以下目录和文件：

- bin  可执行文件目录
- conf 配置文件目录
- html 默认的网站根目录
- logs 日志目录
- start.bat | start.sh 启动脚本

#### 更多信息

- Module Proxy使用手册：源代码仓库的doc目录，或下载发行版时可以找到它

- 这几篇系列文章介绍了Module Proxy的设计思路： [https://my.oschina.net/gzmk/blog/5271822](https://my.oschina.net/gzmk/blog/5271822)

- 配合Module Proxy的Go语言实现的Web后端框架示例： [https://my.oschina.net/gzmk/blog/5343267](https://my.oschina.net/gzmk/blog/5343267)

- Module Proxy的性能测试（和Nginx对比）： [https://my.oschina.net/gzmk/blog/5346893](https://my.oschina.net/gzmk/blog/5346893)

#### 版权及作者

Module Proxy是开源软件，遵循GPL3.

作者：关中麦客 1036038462@qq.com  

关中麦客是一名有20年编程工作的普通程序员，业余时间开发了Module Proxy中间件。他热情欢迎您使用并提出积极的建议，相关的问题请提交到Issue，或直接邮件到 1036038462@qq.com 。
